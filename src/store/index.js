import Vue from 'vue'
import Vuex from 'vuex' 

Vue.use(Vuex)

export const state = {
    previous: null,
    current: '',
    operator: null,
    operatorClicked: false,
    history: '',
    equal: false
}

export const mutations = {
    APPEND_NUMBER: (state, { value }) => {

        state.history = `${state.history}${value}`

        if (state.operatorClicked) {

            state.current = ''
            state.operatorClicked = false
    
        }
    
        state.current = `${state.current}${value}`
        
        state.equal = false
    },

    CALCULATE: (state) => {

        state.current = state.operator(
            parseFloat(state.current), 
            parseFloat(state.previous)
            )
        state.previous = null
        state.equal = true

    },

    ADD : (state) => {

        state.operator = (a, b) => b + a

        state.history = state.history + '+'
        state.previous = state.current
        state.operatorClicked = true
        state.equal = false

    },

    SUBTRACT: (state) => {

        state.operator = (a, b) => b - a

        state.history = state.history + '-'
        state.previous = state.current
        state.operatorClicked = true
        state.equal = false

    },

    DIVIDE: (state) => {

        state.operator = (a, b) => b / a

        state.history = state.history + '/'
        state.previous = state.current
        state.operatorClicked = true
        state.equal = false
    },

    MULTIPLY: (state) => {

        state.history = state.history + '*'
        state.operator = (a, b) => b * a
        state.previous = state.current
        state.operatorClicked = true
        state.equal = false

    },

    CLEAR: (state) => {

        state.history= ''
        state.current= ''
        state.equal = false

    },

    DOT: (state) => {
        
        state.equal = false

        if (state.current.indexOf('.') === -1){

            state.history = state.history + '.'
            state.current = state.current + '.'
    
        }

    },
    SIGN: (state) => {

        state.current = state.current.charAt(0) === '-' ? state.current.slice(1) :  `-${state.current}` ;
        
        state.equal = false

    }
}

export const actions = {

    appendNumber({commit}, value) {
        commit('APPEND_NUMBER', {value})
    },
    add({commit}) {
        commit('ADD')
    },
    substract({commit}) {
        commit('SUBTRACT')
    },
    multiply({commit}) {
        commit('MULTIPLY')
    },
    divide({commit}) {
        commit('DIVIDE')
    },
    clear({commit}) {
        commit('CLEAR')
    },
    calculate({commit}) {
        commit('CALCULATE')
    },
    dot({commit}){
        commit('DOT')
    },
    sign({commit}){
        commit('SIGN')
    },
    // history({commit}, value){
    //     commit('HISTORY', {value})
    // }



}

export default new Vuex.Store({
    state,
    actions,
    mutations
})
